const _trace = (...args) => console.log(...args) || args[0];
const random_sign = () => 0.5 - Math.random();

const walls_canvas = document.getElementById(`walls`);
const units_canvas = document.getElementById(`units`);
const walls_ctx = walls_canvas.getContext(`2d`);
const units_ctx = units_canvas.getContext(`2d`);
const field = [
	`xxxxxxxxxxxxxxxxxx`,
	`x...x........x...x`,
	`x.x...xx..xx...x.x`,
	`x.xx.x......x.xx.x`,
	`x..x...xxxx...x..x`,
	`xx.xx.x    x.xx.xx`,
	`p.....xxxxxx......`,
	`xx.xx........xx.xx`,
	`xx.xxxxx..xxxxx.xx`,
	`x...xx......xx...x`,
	`x.x....xxxx....x.x`,
	`x...xx......xx...x`,
	`xxxxxxxxxxxxxxxxxx`
].map(row => [...row]);

const PACMAN = `\:V`;

const SYMBOLS = {
	WALL: `x`,
	DOT: `.`,
	PAC: `p`,
	GHOST: `g`
};

const board = {
	walls_canvas,
	field,
	WIDTH: 500,
	HEIGHT: 300,
	dimensions: { c: field[0].length, r: field.length },
};
Object.assign(board, {
	cell_width: board.WIDTH / board.dimensions.c,
	cell_height: board.HEIGHT / board.dimensions.r
});

const init_walls_canvas = () => {
	walls_canvas.width = board.WIDTH;
	walls_canvas.height = board.HEIGHT;
	units_canvas.width = board.WIDTH;
	units_canvas.height = board.HEIGHT;
	units_ctx.font = `24px arial black`;
	units_ctx.fillStyle = `black`;
	walls_ctx.font = `36px serif`;
};

const draw_field = () => {
	walls_ctx.clearRect(0, 0, board.WIDTH, board.HEIGHT);
	for (let i = 0; i < board.dimensions.r; ++i) {
		for (let j = 0; j < board.dimensions.c; ++j) {
			const start_y = i * board.cell_height;
			const start_x = j * board.cell_width;
			const mid_x = start_x + board.cell_width / 2;
			const mid_y = start_y + board.cell_height / 2;
			switch(board.field[i][j]) {
				case SYMBOLS.WALL:
					walls_ctx.fillStyle = `rgb(60, 60, 60)`;
					walls_ctx.fillRect(
						start_x,
						start_y,
						board.cell_width,
						board.cell_height
					);
					break;
				case SYMBOLS.DOT:
					walls_ctx.fillStyle = `#fbfb8a`;
					walls_ctx.fillText(
						`.`,
						mid_x,
						mid_y
					);
					break;
			}
		}
	}
};

init_walls_canvas();
draw_field();

const make_ghost = opts => {
	const ghost = {
		...opts,
		steps_left: 16
	};

	return ghost;
};

const crapman = {
	x: 0,
	y: 6,
	dx: 0,
	dy: 0,
	steps_left: 16,
	char: PACMAN,
	dots: 0
};

const grid_compare = symbol => (r, c) => board.field[r][c] === symbol;
const wall = grid_compare(SYMBOLS.WALL);
const dot = grid_compare(SYMBOLS.DOT);

const show_score = score => {
	document.getElementById(`score`).innerHTML = `Points: ${score}`;
};

const geronimo = () => {
	const ghosts = [
		{
			x: 1,
			y: 1,
			dx: 0,
			dy: 1,
			color: `red`
		},
		{
			x: board.field[0].length - 2,
			y: 1,
			dx: -1,
			dy: 0,
			color: `brown`
		},
		{
			x: 6,
			y: 1,
			dx: 1,
			dy: 0,
			color: `green`
		}
	].map(make_ghost);

	const ctx = units_ctx;
	let a = 0;
	const angles = (x, y) => {
		if (x === 1) {
			return 0;
		} else if (y === 1) {
			return Math.PI / 2;
		} else if (x === -1) {
			return Math.PI;
		} else if (y === -1) {
			return Math.PI * 3 / 2;
		} else {
			return a;
		}
	};

	const paint_unit = unit => {
		const offset_x = Math.sign(unit.dx) * ((16 - unit.steps_left) * board.cell_width / 16);
		const offset_y = Math.sign(unit.dy) * ((16 - unit.steps_left) * board.cell_height / 16);

		if (unit.char) {
			a = angles(unit.dx, unit.dy);
			ctx.beginPath();
			ctx.fillStyle = `yellow`;
			ctx.arc(
				(unit.x + 0.5) * board.cell_width + offset_x,
				(unit.y + 0.5) * board.cell_height + offset_y,
				board.cell_height / 2,
				Math.PI / 6 + a,
				Math.PI * 2 - Math.PI / 6 + a
			);
			ctx.lineTo(
				(unit.x + 0.5) * board.cell_width + offset_x,
				(unit.y + 0.5) * board.cell_height + offset_y
			);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			return;
		}

		ctx.fillStyle = unit.color || `red`;
		ctx.beginPath();
		ctx.ellipse(
			(unit.x + .5) * board.cell_width + offset_x,
			(unit.y + .9) * board.cell_height + offset_y,
			board.cell_height * .8,
			board.cell_height / 2,
			Math.PI * .5,
			Math.PI * .5,
			Math.PI * 1.5
		);
		ctx.fill();
	};

	const clear = () => ctx.clearRect(0, 0, board.WIDTH, board.HEIGHT);

	const move_unit = unit => {
		if (unit.steps_left > 0) {
			--unit.steps_left;
			return;
		}

		const deltas = [[unit.dx, unit.dy]].concat([
			[0, 1],
			[1, 0],
			[0, -1],
			[-1, 0]
		].sort(random_sign));

		const [dx, dy] = deltas.find(([dx, dy]) => {
			const c = unit.x + unit.dx + dx;
			const r = unit.y + unit.dy + dy;

			return board.field[r][c] !== SYMBOLS.WALL;
		});

		Object.assign(unit, {
			x: unit.x + unit.dx,
			y: unit.y + unit.dy,
			steps_left: 16,
			dx,
			dy
		});

		if (unit.x < 0) unit.x = board.dimensions.c - 1;
		unit.x = unit.x % board.dimensions.c;
		if (unit.y < 0) unit.y = board.dimensions.r - 1;
		unit.y = unit.y % board.dimensions.r;
	};

	document.body.addEventListener('keydown', event => {
		event.preventDefault();
		const deltas = [
			[-1, 0],
			[0, -1],
			[1, 0],
			[0, 1]
		];

		const new_dx_dy = deltas[event.keyCode - 37];
		if (new_dx_dy) {
			const [dx, dy] = new_dx_dy;
			if (!wall(crapman.y + dy, crapman.x + dx))
				Object.assign(crapman, { dx, dy });
		}
	});

	const move_crapman = (pac, deltas) => {
		if (pac.steps_left > 0) return --pac.steps_left;

		pac.x += pac.dx;
		if (pac.x < 0) pac.x = board.dimensions.c - 1;
		pac.x = pac.x % board.dimensions.c;
		pac.y += pac.dy;
		if (pac.y < 0) pac.y = board.dimensions.r - 1;
		pac.y = pac.y % board.dimensions.r;

		if (dot(pac.y, pac.x)) {
			++pac.dots;
			board.field[pac.y][pac.x] = ``;
		}

		if (!wall(pac.y + pac.dy, pac.x + pac.dx)) {
			pac.steps_left = 16;
		} else {
			pac.dx = 0;
			pac.dy = 0;
		}
	};

	const step = () => {
		clear();
		draw_field();
		for (const g of ghosts) {
			paint_unit(g);
			move_unit(g);
		}
		paint_unit(crapman);
		move_crapman(crapman);
		show_score(crapman.dots);
	};

	const loop = () => {
		step();
		window.requestAnimationFrame(loop);
	};

	loop();
};

geronimo();
